module.exports = {
  mode: 'jit',
  purge: [
   './pages/**/*.{js,ts,jsx,tsx}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: {
          light: "#ffffff",
          DEFAULT: '#323F52',
          dark: "#000000",
        },
        secondary: {
          light:   '#3eb988',
          dark: '#349c73'
        }
      },
      fontFamily: {
        'sans-serif': ['sans-serif']
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
